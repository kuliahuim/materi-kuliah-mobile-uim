import {Text, View, StyleSheet} from 'react-native';
import React, {Component} from 'react';
import CustomInput from '../components/CustomInput';

export class Chat extends Component {
  render() {
    return (
      <View>
        <Text>Chat</Text>
        <CustomInput placeholder={'halo'} />
        <CustomInput multiline={true} />
      </View>
    );
  }
}

export default Chat;

const styles = StyleSheet.create({
  input: {
    borderBottomWidth: 1,
    borderBottomColor: 'lightgreen',
    margin: 10,
  },
});
