import About from './About';
import Chat from './Chat';
import Contact from './Contact';
import Home from './Home';
import Profile from './Profile';

export {About, Home, Chat, Contact, Profile};
