import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import {Home, Profile, About, Chat, Contact} from './pages';
import Icon from 'react-native-vector-icons/Ionicons';

const Stack = createNativeStackNavigator();
const Tabs = createBottomTabNavigator();
const Top = createMaterialTopTabNavigator();

const TopNav = () => {
  return (
    <Top.Navigator>
      <Top.Screen name="Chat" component={Chat} />
      <Top.Screen name="Contact" component={Contact} />
    </Top.Navigator>
  );
};

const Nav = () => {
  return (
    <Tabs.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused
              ? 'ios-information-circle'
              : 'ios-information-circle-outline';
          } else if (route.name === 'Profile') {
            iconName = focused
              ? 'ios-person-circle'
              : 'ios-person-circle-outline';
          } else if (route.name === 'About') {
            iconName = focused ? 'ios-flag' : 'ios-flag-outline';
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: 'tomato',
        tabBarInactiveTintColor: 'gray',
      })}>
      <Tabs.Screen name="Home" component={Home} options={{title: 'Beranda'}} />
      <Tabs.Screen name="About" component={About} />
      <Tabs.Screen name="Message" component={TopNav} />
      <Tabs.Screen name="Profile" component={Profile} />
    </Tabs.Navigator>
  );
};

const Route = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="main"
        component={Nav}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Route;
